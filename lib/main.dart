import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  // image: AssetImage
              )
          ),
          alignment: Alignment.topCenter,
          padding: EdgeInsets.only(left: 15, top: 80, right: 15, bottom: 15),
          // margin: ,
          child: Column(
            children: <Widget>[
              Padding(
                padding:
                EdgeInsets.only(left: 15, top: 0, right: 15, bottom: 0),
                child: Text(
                  "Paris",
                  style: TextStyle(
                    fontSize: 48,
                  ),
                ),
              ),
              Padding(
                padding:
                EdgeInsets.only(left: 15, top: 0, right: 15, bottom: 0),
                child: Text(
                  "48 C",
                  style: TextStyle(
                    fontSize: 96,
                  ),
                ),
              ),
              Padding(
                padding:
                EdgeInsets.only(left: 15, top: 0, right: 15, bottom: 0),
                child: Text(
                  "Clear",
                  style: TextStyle(fontSize: 28),
                ),
              ),
              Padding(
                padding:
                EdgeInsets.only(left: 50, top: 0, right: 15, bottom: 0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          left: 50, top: 0, right: 5, bottom: 0),
                      child: Text(
                        "H:56 C",
                        style: TextStyle(fontSize: 28),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 5, top: 0, right: 5, bottom: 0),
                      child: Text(
                        "L:47 C",
                        style: TextStyle(fontSize: 28),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          // children: <Widget>[
          //   Column(
          //     children: <Widget>[
          //       Container(
          //         width: double.infinity,
          //         height: 300,
          //         color: Colors.indigo,
          //         child: Column(
          //           children: <Widget>[
          //
          //             Text("Paris"),
          //             Text("48 c"),
          //             Column(
          //               children: <Widget>[
          //                 Text("clear"),
          //                 Row(
          //                   children: <Widget>[
          //                     Text("H:56"),
          //                     Text("L:47")
          //                   ],
          //                 )
          //               ],
          //             )
          //           ],
          //         ),
          //
          //       )
          //     ],
          //   )
          // ],
        ),
      ),
    );
  }
}
